function solution(A, K) {
    // write your code in JavaScript (Node.js 6.4.0)
    let removed = [];
    if (K > A.length) {
        K = K % A.length;
    }
    removed = A.splice(-K, K);
    return removed.concat(A);
}