function solution(A) {
    // write your code in JavaScript (Node.js 6.4.0)
    const results = [];
    const firstPartSums = [0];
    const secondPartSums = [0];
    for(let i = 1, l = A.length; i<l; i++) {
        firstPartSums.push(firstPartSums[i-1] + A[i-1]);
        if (l>i) {
            secondPartSums.push(secondPartSums[i-1] + A[l-i]);
        }
    }

    firstPartSums.shift();
    secondPartSums.shift();
    secondPartSums.reverse();
    
    for(let i = 0, l = firstPartSums.length; i<l; i++) {
        results.push(Math.abs(firstPartSums[i] - secondPartSums[i]));
    }
    const min = results.reduce((a, b) => Math.min(a,b));

    return min;
    
}