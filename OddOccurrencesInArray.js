function solution(A) {
    // write your code in JavaScript (Node.js 6.4.0)
    const counter = {};
    for (let i = 0; i<A.length; i++) {
        counter[A[i]] = counter[A[i]] !== undefined ? counter[A[i]]+1 : 1;
    }
    
    let uniq = 0;
    for (let key in counter) {      
        if (counter.hasOwnProperty(key) && counter[key] % 2 !== 0) {
            uniq = key;
        }
    }
    
    return parseInt(uniq, 10);
}
