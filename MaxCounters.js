function solution(N, A) {
    var j;
    var i;
    var len = A.length;
    var lastMax = 0;
    var max = 0; 
    var counters = new Array (N);
    for(j = 0; j < N; j++) counters[j] = 0;
    var n1 = N + 1;
    
    for(j=0; j < len; j++){
        if(A[j] < n1){
            i = A[j] - 1;
            if (counters[i] < lastMax) counters[i] = lastMax;
            counters[i]++;
            if (max < counters[i]) max = counters[i];
        } else {
            lastMax = max;
        }
    }
    
    for(j = 0; j < N; j++){
      if (counters[j] < lastMax) counters[j] = lastMax;
    }
    
    return counters;
}

// slower version
// function solution(N, A) {
//     // write your code in JavaScript (Node.js 6.4.0)
//     const counters = new Array(N);
//     counters.fill(0);
//     let maxCounter = 0;
//     for(let i = 0, l = A.length; i<l; i++) {
//         if (A[i] === N+1) {
//             if (i>0) {
//                 counters.fill(maxCounter);
//             }
//         } else if (A[i] >= 1 && A[i] <= N) {
//             counters[A[i]-1] += 1;
//             if (maxCounter < counters[A[i]-1]) {
//                 maxCounter = counters[A[i]-1];
//             }
//         }
//     }
    
//     return counters;
    
// }