function solution(A) {
    if (!A.length) {
        return 1;
    }
    
    A.sort((a,b)=>a-b);
    
    if (A[0] > 1) {
        return 1;
    }
    
    let test = true;
    let i = 0;
    let missingElement = 0;
    do {
        if (A[i]+1 !== A[i+1]) {
            missingElement = A[i]+1;
            test = false;
        }
        i++;
    } while(test);
    
    return missingElement;
}