function solution(A) {
    A = A.filter(el => el > 0);
    if (!A.length) {
        return 1;
    }
    A.sort((a,b)=> a-b);
    
    var counter = new Array(A[A.length-1]+1);
    counter.fill(0);
    
    // Count the items, only the positive numbers
    for (var i = 0; i < A.length; i++) {
        if (A[i] > 0 && !counter[A[i]+1]) {
            counter[A[i]+1] = 1;
        }
    }
    counter.push(0);
    
    var i = 1;
    while (counter[i+1] !== 0) {
        i++
    }

    return i;
}