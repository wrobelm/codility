// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(N) {
    // write your code in JavaScript (Node.js 6.4.0)
    let binary = N.toString(2);
    const firstOne = binary.indexOf('1');
    const lastOne = binary.lastIndexOf('1');
    
    if (firstOne > 0) {
        binary = binary.substr(firstOne);
    }
    
    if (lastOne !== (binary.length -1)) {
        binary = binary.substr(0, lastOne+1);
    }
    
    const splitted = binary.split('1');
    const counter = [];
    for (let i = 0, l = splitted.length; i<l; i++) {
        counter[i] = splitted[i].length;
    }
    const max = counter.reduce(function(a, b) {
        return Math.max(a, b);
    });
    
    return max;
}